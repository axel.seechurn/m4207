M4207 Projet Antivol de véhicule

L’objectif du projet est de créer un système qui permettra de protéger un véhicule contre le vol.
Pour cela, nous allons utiliser une carte LinkitOne qui va nous permettre de brancher des capteurs pour détecter une infraction,
d’envoyer des informations et des alertes en cas de détection d’intrusion. Il faudra :

- Monitorer l’environnement pour détecter une infraction (détection de mouvement -> accélérometre)

- Envoyer des alertes sur l'interface web en cas de suspicions de  vols -> mouvements 
-
-Enregistrer sa position GPS (Module GPS) et afficher sur l'interface WEB

- Activer/Désactiver alerte sonore (capteur sonore)  depuis l'interface web

- Monitorer le niveau de batterie  afficher son niveau  et lancer une alerte en cas de batterie faible ( Sur Interface Web)


- En mode normal, afficher les informations  sur un écran LCD 
  (Afficher niveau de batterie et afficher "Je sais où vous êtes !" Si détection mouvement =  activé  alerte sonore + localisation GPS
